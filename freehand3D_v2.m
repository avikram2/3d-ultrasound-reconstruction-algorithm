%% freehand 3D ultrasound imaging simulation 
clear all
load('psf_iq.mat')
%% Step 1: Design a phantom 

% object size
x_size = 20;  % mm 
y_size = 20;
z_size = 30; 

% curved surface function 
x = 0:0.1:20;
z = sin((x-12)/3)-2;
z_grad = cos((x-12)/3)/3;

% inside object
% generate two sphere and a cubic 
r1_origin = [10,10,-10];
r1 = 2;
r2_origin = [5,5,-20];
r2 = 3;
c1_origin = [13,12,-16];
c1 = 4;

%% Step 2: Scan generation 
% the scan is in x direction, from 0 to 20, the step is 0.1mm 
img_final = zeros(301,201,100);
for i = 1:200
    i
    %for any point in scan plane, transform from transducer system to world
    %system; 
    scan_origin = [x(i),0,z(i)];
    
    y_T_size = 20;
    z_T_size = 30;
    
    theta = atan(z_grad(i));
    beta = -(theta);
    %R_matrix = [cos(beta),0,sin(beta);0,1,0;-sin(beta),0,cos(beta)];
    
    % generate scatters
    N = 10000;
    z_T = -rand(N,1)*z_T_size;
    y_T = rand(N,1)*y_T_size;
    pos_T = [zeros(length(z_T),1),y_T,z_T];
    pos_w = [sin(beta)*z_T+scan_origin(1), y_T+scan_origin(2), cos(beta)*z_T+scan_origin(3)]; % nX3 array 
    
    % check whether the scatter inside the object 
    z_pos_w = pos_w(:,3);
    pos_w(find(z_pos_w<-z_size),:)=[];
    pos_T(find(z_pos_w<-z_size),:)=[];
    amp = 0.1*abs(randn(size(pos_w,1),1));
    
    mm = 0;
    % check whether the scatter is inside the sphere or cubic 
    for kk = 1:size(pos_w,1)
        if sqrt(sum((pos_w(kk,:)-r1_origin).^2)) <= r1
            amp(kk)=1+abs(randn(1));
        else if sqrt(sum((pos_w(kk,:)-r2_origin).^2)) <= r2
            amp(kk)=0.2+abs(randn(1));
            else if min(c1/2-abs(pos_w(kk,:)- c1_origin))>= 0 
                    amp(kk)=1+0.1*abs(randn(1));
                end
            end
        end
    end
    
    % generate ultrasound image for one scan 
    pix = 0.1;
    img = zeros(301,201);
    y_pix = fix(pos_T(:,2)/pix)+1;
    z_pix = fix(abs(pos_T(:,3))/pix)+1;
    for kkk = 1:length(y_pix)
        img(z_pix(kkk)+size(img,1)*(y_pix(kkk)-1))= amp(kkk) + img(z_pix(kkk)+size(img,1)*(y_pix(kkk)-1));
    end
    
    img_final(:,:,i) = img;
end 

% image display
figure
for i = 1:200
    imagesc([0:0.1:20],[-30:0.1:0],img_final(:,:,i))
    colormap('hot')
    colorbar
    axis image
    xlabel('mm')
    ylabel('mm') 
    hold on
    pause(0.2)
end

% generate ultrasound image 
psf_test = psf_iq;
img_ultrasound = img_final;
for kk = 1:200
    kk
    img = img_final(:,:,kk);
    Nx_psfh = fix(size(psf_test,2)/2);
    Ny_psfh = fix(size(psf_test,1)/2);
    [y_pix,x_pix] = find(img);
    amp = img(find(img));
    for i = 1:length(x_pix) 
        x0 = x_pix(i);
        y0 = y_pix(i);
        y1 = max(1,y0-Ny_psfh);
        y2 = min(size(img,1),y0+Ny_psfh);
        x1 = max(1,x0-Nx_psfh);
        x2 = min(size(img,2),x0+Nx_psfh);
        img(y1:y2,x1:x2) = img(y1:y2,x1:x2)+amp(i)*psf_test((y1-y0+Ny_psfh+1):(y2-y0+Ny_psfh+1),(x1-x0+Nx_psfh+1):(x2-x0+Nx_psfh+1));
    end 
    img_ultrasound(:,:,kk) = img;
end

figure
wl = 1.54/7.4;
x_axis = linspace(-0.5,0.5,size(img_ultrasound,2))*size(img_ultrasound,2)*0.5/4*wl;
z_axis = linspace(0,1,size(img_ultrasound,1))*size(img_ultrasound,1)*0.5/4*wl;
for i = 1:200
    imagesc(x_axis,z_axis,abs(img_ultrasound(:,:,i)))
    colormap('gray')
    colorbar
    axis image
    xlabel('mm')
    ylabel('mm') 
    hold on
    pause(0.1)
end

%% Step 3: project to 3D image based on scanning slides 
% using volume interpolation based on median filter 
x_volumn = 0.1;
y_volumn = 0.1;
z_volumn = 0.1;

z_volumn_voxel = 0:-0.1:-30; 
y_volumn_voxel = 0:0.1:20;
x_volumn_voxel = 0:0.1:20; 

volumn_final = zeros(301,201,201);  %% z,y,x 
[y_T_img,z_T_img] = meshgrid(1:201,1:301);
y_T_img = (y_T_img(:)-1)*pix;
z_T_img = -(z_T_img(:)-1)*pix;

voxel_value = []; 
R_threshould = 0.1; 
volumn_weight = 0.01*ones(301,201,201);

tic
for i = 1:200
    i
    scan_origin = [x(i),0,z(i)];
    theta = atan(z_grad(i));
    beta = -(theta);
    
    x_w_img = sin(beta)*z_T_img+scan_origin(1);
    y_w_img = y_T_img+scan_origin(2);
    z_w_img = cos(beta)*z_T_img+scan_origin(3); 
    img = abs(img_ultrasound(:,:,i)); %img_final(:,:,i);
%     img = img/max(img(:))*255;
    img_array = img(:);
    
    % the size restriction: 0<x_w<20, -30<z_w<0 
    inside_ = x_w_img>0&x_w_img<20&z_w_img<0&z_w_img>-30;
    img_array = img_array(inside_);
    x_w_img = x_w_img(inside_);
    y_w_img = y_w_img(inside_);
    z_w_img = z_w_img(inside_);
    
    % find the voxel value by calculating the distance between the voxel
    % and pixel; then store all distances and intensity of that voxel to
    % an array and apply median filter later. 
    voxel_value = [];
    x_in = (abs(x_w_img-x_volumn_voxel)<R_threshould);
    y_in = (abs(y_w_img-y_volumn_voxel)<R_threshould);
    z_in = (abs(z_w_img-z_volumn_voxel)<R_threshould);
    for j1 = 1:length(x_w_img)
%         j1
        k1 = find(x_in(j1,:)==1)';
        k2 = find(y_in(j1,:)==1)';
        k3 = find(z_in(j1,:)==1)';
        dis_x = [x_volumn_voxel(k1)-x_w_img(j1)]; 
        dis_y = [y_volumn_voxel(k2)-y_w_img(j1)]; 
        dis_z = [z_volumn_voxel(k3)-z_w_img(j1)]; 
        dis_ = repmat(dis_x',[1 length(k2) length(k3)]).^2+permute(repmat(dis_y',[1 length(k1) length(k3)]).^2,[2,1,3])+...
            +permute(repmat(dis_z',[1 length(k1) length(k2)]).^2,[2,3,1]);
        a = reshape(dis_(dis_<R_threshould^2),[],1)*100;
        a(a==0)=1e-10; %avoid zero value 
        c = find (dis_<R_threshould^2);
        [index_x,index_y,index_z] = ind2sub(size(dis_),c);
        volumn_final(k3(index_z(:))+(k2(index_y(:))-1)*301*201+(k1(index_x(:))-1)*301) = ...
            volumn_final(k3(index_z(:))+(k2(index_y(:))-1)*301*201+(k1(index_x(:))-1)*301)+img_array(j1)./sqrt(a);
        volumn_weight(k3(index_z(:))+(k2(index_y(:))-1)*301*201+(k1(index_x(:))-1)*301) = ...
            volumn_weight(k3(index_z(:))+(k2(index_y(:))-1)*301*201+(k1(index_x(:))-1)*301)+1./sqrt(a);
%         voxel_value = [voxel_value;[k1(index_x(:)),k2(index_y(:)),k3(index_z(:)),sqrt(a),ones(length(index_x(:)),1)*img_array(j1)]];
    end  
%     %%%%%%%%%%%%%%%%%%%%%%%%% 
%     %%%brute force version    
%     for j1 = 1:length(x_volumn_voxel)
%         for j2 = 1:length(y_volumn_voxel)
%             for j3 = 1:length(z_volumn_voxel)
%                 voxel_index = j3 + (j2-1)*301*201+(j1-1)*301;
%                 R_2 = (x_w_img - x_volumn_voxel(j1)).^2+(y_w_img - y_volumn_voxel(j2)).^2+(z_w_img - z_volumn_voxel(j3)).^2;
%                 if isempty(R_2)~=0
%                     intensity = img_array(find(R_2<R_threshould^2));
%                     dis = sqrt(R_2(find(R_2<R_threshould^2)));
%                     voxel_value = [voxel_value;voxel_index*ones(length(dis),1),intensity,dis];
%                 end 
%             end
%         end
%     end
%     %%%%%%%%%%%%%%%%%%%%%%%%%%
end 
volumn_final = volumn_final./volumn_weight;

timeElapsed = toc
