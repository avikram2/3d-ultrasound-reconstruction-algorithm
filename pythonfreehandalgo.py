
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#import libraries


# object size
x_size = 20  # mm
y_size = 20
z_size = 30



#curved surface function
x = np.arange(0, 20.1, 0.1)
z = (np.sin((x-12)/3)-2)
z_grad = (np.cos((x-12)/3)/3)

#generating two spheres and cube

r1_origin = np.array([10,10,-10])
r1 = 2
r2_origin = np.array([5,5,-20])
r2 = 3
c1_origin = np.array([13,12,-16])
c1 = 4

#Scan Generation

img_final = np.zeros(301, 201, 100)

for x in range(200):
    scan_origin = np.array([x[i], 0, z[i]])
    
    y_T_size = 20
    z_T_size = 30
    

    theta = np.arctan(z_grad[i])
    beta = -1 * theta
    
    #scatters
    N = 10000
    z_T = -1 * np.random.rand(N, 1) * z_T_size
    y_T = np.random.rand(N, 1) * y_T_size 
    pos_T = np.array([np.zeros((len(z_T), 1)), y_T, z_T])
    pos_w = np.array([np.sin(beta)*z_T+scan_origin[0], y_T+scan_origin[1], np.cos(beta)*z_T+scan_origin[2]])
    
    # Check if scatter inside object
    z_pos_w = pos_w[:,3]
    pos_w[np.argwhere(z_pos_w<(-1*z_size)), :] = []
    pos_T[np.argwhere(z_pos_w < (-1*z_size)),:] = []
    amp = 0.1*np.abs(np.random.randn(pos_w.size[0]))
    
   
    mm = 0
    for kk in range(200):
    	if np.sqrt(np.sum(np.linalg.matrix_power((pos_w[kk,:]-r1_origin), 2))) <= r1:
	    amp[kk] = 1+np.abs(np.random.randn(1))
	elif np.sqrt(np.sum(np.linalg.matrix_power((pos_w[kk,:]-r2_origin), 2))) <= r2:
	    amp[kk] = 0.2+np.abs(np.random.randn(1))

